from circle2d import *

def circle1():
    circle1.up()
    circle1.goto(2, 2)
    circle1.down()
    circle1.width(11)

def circle2():
    circle2.up()
    circle2.goto(4, 5)
    circle2.down()
    circle2.width(21)

def circle3():
    circle3.up()
    circle3.goto(3, 5)
    circle3.down()
    circle3.width(4.6)

print(getArea(circle1))
print(getPerimeter(circle1))
print(getArea(circle2))
print(getPerimeter(circle2))
print(getArea(circle3))
print(getPerimeter(circle3))
print(containsPoints(3, 3))
print(contains(c2))
print(overlaps(c3))
